package me.singularex.filestorage

import java.net.URI
import java.nio.ByteBuffer
import java.net.URL
import scala.util.Try
import scala.concurrent.Future


trait FileStorage {
  
  /** Load file if exist */
  def load(namespace: String, fileId: String, withEmptyContent: Boolean = false): Future[Option[File]]
  
  /** Save file */
  def save(file: File): Future[Unit]
  
  /** Delete file if exist and silently skip if not*/  
  def delete(namespace: String, fileId: String): Future[Unit]
  
  /** Return direct downloading url to file, can be provided to any internet clients
   *  Some `FileStorage` implementations can't have external access and throw `NotImplementedError`
   */
  def getFileExternalUrl(namespace: String, fileId: String): Future[URL]
}
