package me.singularex.filestorage

import java.util.UUID
import java.io.InputStream
import java.io.FileInputStream
import java.io.ByteArrayInputStream
import me.singularex.utils.stream.DownloadUtils

/**
 * File record that can be stored/loaded in storage
 * 
 * `namespace` + `fileId` identify specific file and should be unique. 
 * It is possible to keep file extention in `fileId` like '23453467234523456.jpg'
 * 
 * `content` represent file content, byte array and stream are supported transparently.
 * Most of `FileStorage` implementation can get benefits from stream content and avoid loading content to memory
 * 
 * ''Note:'' InputStream is mutable structure with noticeable side-effect. It can't be read several times 
 * 
 * `mimeType` is as meta information together with file
 * 
 * `meta` - any information, is stored together with file
 */
case class File (
  namespace: String,
  fileId: String,
  content: FileContent,
  mimeType: String,
  meta: Option[Map[String, String]] = None
)


class FileContent protected (
  fc: Either[Array[Byte], java.io.InputStream]   
) {
  lazy val byteArray: Array[Byte] = fc match {
    case Left(ba) => 
      ba
    case Right(is) => 
      val rez = DownloadUtils.download(is)
      is.close()
      rez
  }
  
  val stream: InputStream = fc match {
    case Left(ba) => new ByteArrayInputStream(ba)
    case Right(is) => is
  }
  
  override def toString() = fc match {
     case Left(ba) => s"ByteArray(${ba.take(10).mkString(",")})"
    case Right(is) => s"Stream($is)"
  }
}


object FileContent {
  def apply(a: Array[Byte]) = new FileContent(Left(a))
  def apply(is: InputStream) = new FileContent(Right(is))
  def apply(file: java.io.File) = new FileContent(Right(new FileInputStream(file)))  
}

