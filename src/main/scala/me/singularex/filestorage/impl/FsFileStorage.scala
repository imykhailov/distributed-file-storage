package me.singularex.filestorage.impl

import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.net.URL
import me.singularex.filestorage.File
import me.singularex.filestorage.Utils
import me.singularex.filestorage.FileStorage
import me.singularex.utils.stream.DownloadUtils
import scala.util.Try
import play.api.libs.json._
import me.singularex.utils.ExtendedTry._
import java.nio.file.NoSuchFileException
import me.singularex.filestorage.FileContent
import scala.concurrent._
import ExecutionContext.Implicits.global
import org.slf4j.LoggerFactory


class FsFileStorage(
  fileStorageLocalPath: String    
) extends FileStorage {
  
  val Logger = LoggerFactory.getLogger(this.getClass);
  Logger.info(s"Filesystem file storage started, path: '$fileStorageLocalPath'")

  def fullFileName(namespace: String, fileId: String) = fileStorageLocalPath.stripSuffix("/") + "/" + namespace + "/" + fileId
  
  def fileMetaPath(namespace: String, fileId: String) = fullFileName(namespace, fileId) + ".meta"

    override def load(namespace: String, fileId: String, withEmptyContent: Boolean = false): Future[Option[File]] = {
      future { 
        try {
          val content = if (withEmptyContent) {
            Array.empty[Byte]
          } else {
            
            DownloadUtils.download(fullFileName(namespace, fileId))
          }
          val metac = DownloadUtils.download(fileMetaPath(namespace, fileId))
          val meta = Json.parse(metac).\("meta") match {
            case JsObject(meta) => meta.toMap[String, JsValue].mapValues(_.as[String])
            case _ => Map.empty[String, String]
          }
          val mimeType = Json.parse(metac).\("mimeType") match {
            case JsString(mimeType) => mimeType
            case _ => Utils.guessMimeType(fileId)
          }
          
          Some(File(namespace, fileId, FileContent(content), mimeType, Some(meta)))
        } catch {
          case e: FileNotFoundException => None
          case e: NoSuchFileException => None
        }
      }
    }

    override def save(file: File): Future[Unit] = {
      val f = new java.io.File(fullFileName(file.namespace, file.fileId));
      f.getParentFile().mkdirs();
      val out = new FileOutputStream(f)
      val metaf = new FileOutputStream(fileMetaPath(file.namespace, file.fileId))
      val rez = future {
        val meta = file.meta
        val input = file.content.stream
        Iterator 
          .continually(input.read())
          .takeWhile (_ != -1)
          .foreach (out.write)
        input.close()
        out.close()
        metaf.write(Json.obj(
          "meta" -> file.meta,
          "mimeType" -> file.mimeType
        ).toString.getBytes())
      } 
      
      rez.onComplete { foo => 
        out.close()
        metaf.close()        
      }
      
      rez
    }

    override def delete(namespace: String, fileName: String): Future[Unit] = {
      future {
        (new java.io.File(fullFileName(namespace, fileName))).delete()
        (new java.io.File(fileMetaPath(namespace, fileName))).delete()
      } 
    }

    override def getFileExternalUrl(namespace: String, fileName: String): Future[URL] = 
      throw new NotImplementedError()

}
