package me.singularex.filestorage.impl

import java.nio.ByteBuffer
import me.singularex.filestorage.File
import java.net.URI
import java.net.URL
import java.util.UUID
import play.api.libs.json._
import scala.concurrent.duration._
import scala.concurrent.Await
import scala.concurrent.Future
import me.singularex.utils.MD5
import me.singularex.filestorage.Utils
import me.singularex.filestorage.FileStorage
import scala.util.{Try, Success, Failure}
import scala.concurrent.ExecutionContext
import com.ning.http.client.AsyncHttpClientConfig
import com.ning.http.client.AsyncHttpClient
import com.ning.http.client.Response
import com.ning.http.client.ListenableFuture
import scala.concurrent.Promise
import scala.concurrent.ExecutionContextExecutor
import scala.util.control.NonFatal
import me.singularex.filestorage.FileContent
import org.slf4j.LoggerFactory
import java.util.concurrent.Executor
import scala.util.Failure


/**
 * File storage with nginx backend. Use WebDab for file modifications, currently support only one nginx server
 * 
 * `nginxPublicUrl` public path to storage root. Should be accessible from internet and SHOULD NOT support modification 
 * requests PUT, POST, DELETE 
 * 
 * `nginxWebDavUrl` internal webdav path to storage root, should be accessible only for application server and should 
 * support PUT and DELETE operations
 * 
 * `httpClientConfig` ning http client configuration. It is optional, if none then default will be used
 */
class NginxFileStorage(
  nginxPublicUrl: URL,
  nginxWebDavUrl: URL,
  httpClientConfig: Option[AsyncHttpClientConfig] = None,
  val numberOfMicroshards: Int = 1024 
)(
  implicit executionContext: ExecutionContext   
) extends FileStorage {
  
  val Logger = LoggerFactory.getLogger(this.getClass);
      
  val NOT_FOUND = 404
  val OK = 200
  
  class FileStorageException(message: String, cause: Exception = null) extends Exception(message, cause)
  
  def defaultHttpClientConfig = (new AsyncHttpClientConfig.Builder)
    .setConnectionTimeoutInMs(5000)
    .setRequestTimeoutInMs(5000)
    .setIdleConnectionTimeoutInMs(5000)
    .setIdleConnectionInPoolTimeoutInMs(5000)
    .build()
  
  val asyncHttpClient = new AsyncHttpClient(httpClientConfig.getOrElse(defaultHttpClientConfig))

  val timeout: Duration = 1 minute
  
  testConnections()

  def microshard(fileId: String): String = MD5.digestToBigInt(fileId).mod(numberOfMicroshards).toString

  protected def fileUrl(baseUrl: URL, namespace: String, fileId: String): String = {
    val path = namespace + "/" + microshard(fileId) + "/" + fileId
    baseUrl.toString.stripSuffix("/") + "/" + path
  }

  protected def metaUrl(baseUrl: URL, namespace: String, fileId: String): String = fileUrl(baseUrl, namespace, fileId) + ".meta"

  protected def format(namespace: String, fileId: String): String = s"file[namespace=${namespace}, id=${fileId}]"
  protected def format(file: File): String = format(file.namespace, file.fileId)
  protected def format(resp: Response): String = s"Response[status=${resp.getStatusCode()}, body=${resp.getResponseBody().take(1000)}]"

  protected def checkResul
    (operation: String, isError: Response => Boolean)
    (futures: Seq[Future[Response]], namespace: String, fileId: String): Future[Unit] = {
    
    Future.sequence(futures).map { rezSeq =>
      val errors = rezSeq.flatMap { resp =>
        if (isError(resp)) {
          List(s"Error in ${operation} file ${format(namespace,fileId)}]. Reason: ${format(resp)}")
        } else {
          Nil
        }
      }
      if (!errors.isEmpty) throw new FileStorageException(errors.mkString("\n\n"))
    }
  }

  protected def checkSavingError = checkResul("saving", { resp: Response => resp.getStatusCode()/100 != 2 }) _
  protected def checkRemovingError = checkResul("removing", { resp: Response => resp.getStatusCode()/100 != 2 && resp.getStatusCode() != NOT_FOUND }) _

  protected def extractMetaAndType(namespace: String, fileId: String, resp: Response): (Map[String, String], String) = {
    if (resp.getStatusCode() == OK) {
      val json = Json.parse(resp.getResponseBodyAsBytes())
      val meta = json.\("meta") match {
        case JsObject(meta) => meta.toMap[String, JsValue].mapValues(_.as[String])
        case JsNull => Map.empty[String, String]
        case _ => {
          Logger.error(s"Can't load meta for ${format(namespace,fileId)}]. Reason: wrong json, ${format(resp)}")
          Map.empty[String, String]
        }
      }        
      val mimeType = json.\("mimeType") match {
        case JsString(mimeType) => mimeType
        case JsNull => Utils.guessMimeType(fileId)
        case _:JsUndefined => Utils.guessMimeType(fileId) 
        case _ => {
          Logger.error(s"Can't load mimeType for ${format(namespace,fileId)}]. Reason: wrong json, ${format(resp)}")
          Utils.guessMimeType(fileId)
        }
      }
      (meta, mimeType)
    } else {
      Logger.error(s"Can't load meta for ${format(namespace,fileId)}]. Reason: " + resp)
      (Map.empty, Utils.DefaultMimeType)
    }
  }
  
  
  override def load(namespace: String, fileId: String, withEmptyContent: Boolean = false): Future[Option[File]] = {

      val contentFutureO = if (withEmptyContent) {
        Future.successful(None)
      } else {
        ningFutureConvert(asyncHttpClient.prepareGet(fileUrl(nginxWebDavUrl, namespace, fileId)).execute()).map(r => Some(r))
      }
    
      val metaFuture = ningFutureConvert(asyncHttpClient.prepareGet(metaUrl(nginxWebDavUrl, namespace, fileId)).execute())

      for {
        contentResp <- contentFutureO
        metaResp <- metaFuture
      } yield {
        contentResp.map(_.getStatusCode()) match {
          case Some(NOT_FOUND) => None
          case Some(OK) => {
            val (meta, mimeType) = extractMetaAndType(namespace, fileId, metaResp)
            Some(File(namespace, fileId, FileContent(contentResp.get.getResponseBodyAsStream()), mimeType, Some(meta)))
          }
          case None => {
            val (meta, mimeType) = extractMetaAndType(namespace, fileId, metaResp)
            Some(File(namespace, fileId, FileContent(Array.empty[Byte]), mimeType, Some(meta)))
          }
          case _ => throw new Exception("Something is wrong with Nginx: " + contentResp)
        }
      }
  }

  override def save(file: File): Future[Unit] = {
    val meta = Json.obj(
      "meta" -> file.meta,
      "mimeType" -> file.mimeType
    ).toString.getBytes()
    checkSavingError(
      ningFutureConvert(asyncHttpClient.preparePut(fileUrl(nginxWebDavUrl, file.namespace, file.fileId)).setBody(file.content.stream).execute()) ::
      ningFutureConvert(asyncHttpClient.preparePut(metaUrl(nginxWebDavUrl, file.namespace, file.fileId)).setBody(meta).execute()) :: Nil,        
      file.namespace,
      file.fileId
    )
  }

  override def delete(namespace: String, fileId: String): Future[Unit] = {      
    checkRemovingError(
      ningFutureConvert(asyncHttpClient.prepareDelete(fileUrl(nginxWebDavUrl, namespace, fileId)).execute()) ::
      ningFutureConvert(asyncHttpClient.prepareDelete(metaUrl(nginxWebDavUrl, namespace, fileId)).execute()) :: Nil,
      namespace,
      fileId
    )
  }

  override def getFileExternalUrl(namespace: String, fileId: String): Future[URL] = Future.successful(new URL(fileUrl(nginxPublicUrl, namespace, fileId)))
  
  protected lazy val executor = new Executor {
    def execute(runnable: Runnable): Unit = {
      executionContext.execute(runnable)
    }
  }
  
  protected def ningFutureConvert[T](nf: ListenableFuture[T]): Future[T] = {
    val rez = Promise[T]()
    nf.addListener(new Runnable {
      def run = rez.complete(Try{nf.get()})
    }, executor)
    rez.future
  } 
  
  def testConnections(): Unit = {
    
    def checkUrlAccessibility(url: URL): Future[(Boolean, String)] = {
      val f = ningFutureConvert(asyncHttpClient.prepareGet(url.toString()).execute())
      f.map (rez => {
        if (rez.getStatusCode() == 200) {
          (true, "OK")
        } else {
          (false, s"Error. Response code: ${rez.getStatusCode()}(${rez.getStatusText()})")
        }        
      }).recover({
        case NonFatal(ex) => (false, s"Error. Exception: ${ex.getMessage()})")
      })
    }
    
    val webDavF = checkUrlAccessibility(nginxWebDavUrl)
    val publicF = checkUrlAccessibility(nginxPublicUrl)
    
    for {
      webDavRez <- webDavF
      publicRez <- publicF
    } {
      val msg = s"""
        |Nginx file storage started.
        |  publicUrl: '$nginxPublicUrl' ${publicRez._2}
        |  webDavUrl: '$nginxWebDavUrl' ${webDavRez._2}""".stripMargin
      
      if (webDavRez._1 && publicRez._1) {
        Logger.info(msg)
      } else {
        Logger.error(msg)
      }
    }
  }
}
