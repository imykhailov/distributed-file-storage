package me.singularex.filestorage.impl

import me.singularex.filestorage.FileStorage
import com.datastax.driver.core.querybuilder.QueryBuilder
import java.nio.ByteBuffer
import java.net.URI
import me.singularex.filestorage.File
import java.net.URL
import java.util.UUID

/*
class CassandraFileStorage (val cassandra: CassandraConnectionProvider) extends FileStorage {
    val FILE_TABLE = "file"
      
    implicit def session = cassandra.session

    import storum.cassandra.CassandraMapper._

    def byteBufferToArray(bb: ByteBuffer): Array[Byte] = {
      val rez = new Array[Byte](bb.remaining())
      bb.get(rez)
      rez
    }

    override def find(namespace: String, fileId: String, withEmptyContent: Boolean = false): Option[File] = {
      if (withEmptyContent) {
        val query = QueryBuilder
          .select(fileDescriptionWithoutContect.columnNames: _*)
          .from(FILE_TABLE)
          .where(QueryBuilder.eq(wrapInQuotes("fileId"), fileId))
          .and(QueryBuilder.eq(wrapInQuotes("namespace"), namespace))
        execute(query).mapFirst(fileParserWithoutContect)
      } else {
        val query = QueryBuilder
          .select(fileDescription.columnNames: _*)
          .from(FILE_TABLE)
          .where(QueryBuilder.eq(wrapInQuotes("fileId"), fileId))
          .and(QueryBuilder.eq(wrapInQuotes("namespace"), namespace))
        execute(query).mapFirst(fileParser)
      }
    }

    override def save(file: File): ServiceResult[Unit] = {
      val query = fileSaver(QueryBuilder.insertInto(FILE_TABLE), file)
      execute(query)
      ServiceResult.success()
    }

    override def delete(namespace: String, fileId: String): ServiceResult[Unit] = {
      val query = QueryBuilder
        .delete()
        .from(FILE_TABLE)
        .where(QueryBuilder.eq(wrapInQuotes("fileId"), fileId))
        .and(QueryBuilder.eq(wrapInQuotes("namespace"), namespace))
      execute(query)
      ServiceResult.success()
    }

    override def getFileExternalUrl(namespace: String, fileName: String): URL = {
      new URL("/items/image/" + fileName)
    }

    protected val fileDescriptionWithoutContect =
      get[String]("namespace") ~
      get[String]("fileId") ~
      get[String]("mimeType") ~
      getOptional[Map[String,String]]("meta")
      
    protected val fileParserWithoutContect = fileDescriptionWithoutContect parser {
      case namespace ~ fileId ~ mimeType ~ meta => File(namespace, fileId, Array.empty, mimeType, meta)
    }
    
    protected val fileDescription =
      get[String]("namespace") ~
      get[String]("fileId") ~
      get[Array[Byte]]("content", default = Array.empty[Byte]) ~
      get[String]("mimeType") ~
      getOptional[Map[String,String]]("meta")

    protected val fileParser = fileDescription parser {
      case namespace ~ fileId ~ content ~ mimeType ~ meta => File(namespace, fileId, content, mimeType, meta)
    }

    protected val fileSaver = fileDescription saver { (f: File) =>
      f.namespace ~ f.fileId ~ f.content ~ f.mimeType ~ f.meta
    }

}
*/
