package me.singularex.utils

import java.io.ByteArrayInputStream
import javax.imageio.ImageIO
import scala.util.Try
import scala.util.Success
import scala.util.Failure

object FileUtils {

  case class ImageSizes(width: Int, height: Int)
  
  def getImageSize(image: Array[Byte]): Option[ImageSizes] = {    
    val in = new ByteArrayInputStream(image)
    Try(Option(ImageIO.read(in))) match {
      case Success(bImage) => bImage.map(image => ImageSizes(image.getWidth(), image.getHeight()))
      case Failure(_) => None
    }
  }
}