package me.singularex.utils

import scala.math.BigInt.javaBigInteger2bigInt

object MD5 {
  private[this] val HEX = 16

  def digest(s: String): String = {
    digest(s.getBytes("UTF-8"))
  }

  def digest(b: Array[Byte]): String = {
    digestToBigInt(b).toString(HEX)
  }

  def digestToBigInt(b: Array[Byte]): BigInt = {
    val m = java.security.MessageDigest.getInstance("MD5")
    m.update(b, 0, b.length)
    new java.math.BigInteger(1, m.digest())
  }

  def digestToBigInt(s: String): BigInt = {
    digestToBigInt(s.getBytes("UTF-8"))
  }
}
