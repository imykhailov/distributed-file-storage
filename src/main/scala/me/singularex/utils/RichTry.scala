package me.singularex.utils

import scala.util.Try


object ExtendedTry {

  implicit class ExtendedTry[T](baseTry: Try[T]) {
    def finallyDo(code: => Unit): Try[T] = baseTry.transform(foo => {code; baseTry}, bar => {code; baseTry})
  }
}