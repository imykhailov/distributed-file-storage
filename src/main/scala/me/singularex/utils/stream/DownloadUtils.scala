package me.singularex.utils.stream

import java.io.InputStream
import java.net.URL
import java.nio.file.{Files, Paths}
import java.net.URI

object DownloadUtils {
  
  def download(filePath: String): Array[Byte] = Files.readAllBytes(Paths.get(filePath))
  
  def download(url: URL): Array[Byte] = {
    val s = url.openStream()
    try {download(s)} finally {s.close()}
  }
  
  def download(stream: InputStream): Array[Byte] = {
    val rez = Stream.continually(stream.read).takeWhile(-1 !=).map(_.toByte).toArray
    stream.close()
    rez
  }
  
}
