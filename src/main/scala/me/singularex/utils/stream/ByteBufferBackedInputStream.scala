package me.singularex.utils.stream

import java.nio.ByteBuffer
import java.io.InputStream

class ByteBufferBackedInputStream(buf: ByteBuffer) extends InputStream {

    override def read(): Int =
      if (!buf.hasRemaining()) {
        -1
      } else {
        buf.get() & 0xFF
      }

    override def read(bytes: Array[Byte], off: Int, len: Int) =
      if (!buf.hasRemaining()) {
          -1
      } else {
        val rlen = Math.min(len, buf.remaining())
        buf.get(bytes, off, rlen)
        rlen
      }
}
