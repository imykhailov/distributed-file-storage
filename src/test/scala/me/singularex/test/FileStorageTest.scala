package me.singularex.test

import java.util.UUID
import org.specs2.mutable
import java.nio.ByteBuffer
import org.specs2.matcher._
import scala.collection.JavaConverters._
import me.singularex.filestorage.File
import java.net.URL
import me.singularex.utils.stream.DownloadUtils
import me.singularex.utils.MD5
import scala.concurrent.Await
import scala.concurrent.duration.`package`.DurationInt
import scala.util.{Try, Success}
import me.singularex.filestorage.FileStorage
import com.ning.http.client.AsyncHttpClientConfig
import com.ning.http.client.AsyncHttpClient
import me.singularex.filestorage.impl.NginxFileStorage
import scala.concurrent.ExecutionContext.Implicits.global
import me.singularex.filestorage.impl.FsFileStorage
import me.singularex.filestorage.FileContent
import scala.concurrent.Future
import scala.concurrent.duration.Duration
import me.singularex.filestorage.impl.NginxFileStorage


abstract class FileStorageTest extends mutable.SpecificationWithJUnit {

  def fileStorage: FileStorage
  
  def isFileStorageAvailable: Boolean
  def skipIfNotAvailable() = if (!isFileStorageAvailable) skipped("Filestorage is not available")
  
  def await[T](f: Future[T]): T = Await.result(f, Duration(10, "seconds"))
      
  lazy val testFileContent = DownloadUtils.download(this.getClass().getResourceAsStream("/default_item.jpeg"))
  
  val testNamespace = "FileStorageTestNamespace"
  
  "Test file" should {
    "be correct" in {
      MD5.digest(testFileContent) must beEqualTo("f4526db885966605fe80cb15e02af94f")
    }
  }
  
  "File" should {
    "be created" in {
      skipIfNotAvailable() 
      val file = File(testNamespace, UUID.randomUUID.toString, FileContent(testFileContent), "image/jpeg")
      await(fileStorage.save(file))
      success
    }
    
    "be loaded" in {
      skipIfNotAvailable() 
      val fileName = UUID.randomUUID.toString
      val file = File(testNamespace, fileName, FileContent(testFileContent), "image/jpeg")
      await(fileStorage.save(file))
      val loaded = await(fileStorage.load(testNamespace, fileName))
      loaded must beSome
      MD5.digest(loaded.get.content.byteArray) must beEqualTo("f4526db885966605fe80cb15e02af94f")
    }
    
    "be None if not exist" in {
      skipIfNotAvailable() 
      val fileName = UUID.randomUUID.toString
      val loaded = await(fileStorage.load(testNamespace, fileName))
      loaded must beNone
    }
    
    "be loaded without content if required" in {
      skipIfNotAvailable() 
      val fileName = UUID.randomUUID.toString
      val file = File(testNamespace, fileName, FileContent("someTest".getBytes()), "image/jpeg")
      await(fileStorage.save(file))
      val loaded = await(fileStorage.load(testNamespace, fileName, withEmptyContent = true))
      loaded must beSome
      loaded.get.content.byteArray must beEqualTo(Array.empty)
    }
    
    "be deleted" in {
      skipIfNotAvailable() 
      val fileName = UUID.randomUUID.toString
      val file = File(testNamespace, fileName, FileContent(testFileContent), "image/jpeg")
      await(fileStorage.save(file))
      val loaded = await(fileStorage.load(testNamespace, fileName)).get
      MD5.digest(loaded.content.byteArray) must beEqualTo("f4526db885966605fe80cb15e02af94f")
      await(fileStorage.delete(testNamespace, fileName))
      await(fileStorage.load(testNamespace, fileName)) must beNone
    }
    
    "be deleted several times without error" in {
      skipIfNotAvailable() 
      val fileName = UUID.randomUUID.toString
      val file = File(testNamespace, fileName, FileContent(testFileContent), "image/jpeg")
      await(fileStorage.save(file))
      val loaded = await(fileStorage.load(testNamespace, fileName)).get
      MD5.digest(loaded.content.byteArray) must beEqualTo("f4526db885966605fe80cb15e02af94f")
      await(fileStorage.delete(testNamespace, fileName))
      await(fileStorage.load(testNamespace, fileName)) must beNone
      await(fileStorage.delete(testNamespace, fileName))
      await(fileStorage.load(testNamespace, fileName)) must beNone
    }
    
    "keep mimeType" in {
      skipIfNotAvailable() 
      val fileName = UUID.randomUUID.toString
      val mimeType = "image/jpeg"
      val file = File(testNamespace, fileName, FileContent(testFileContent), "image/jpeg")
      await(fileStorage.save(file))
      val loaded = await(fileStorage.load(testNamespace, fileName)).get
      loaded.mimeType must beEqualTo(mimeType)
    }
    
    "store and provide meta information" in {
      skipIfNotAvailable() 
      val fileName = UUID.randomUUID.toString
      val meta = Map(
        "size" -> "50x70",
        "extention" -> "jpeg"
      )
      val file = File(testNamespace, fileName, FileContent(testFileContent), "image/jpeg", Some(meta))
      await(fileStorage.save(file))
      
      await(fileStorage.load(testNamespace, fileName)).get.meta must beSome(meta)
    }
  }
}


/*
class CassandraFileStorageTest extends FileStorageTest {
  override def config: Map[String, Any] = Map(
    "filestorage.type" -> "cassandra"
  )
    
  override def isFileStorageAvailable: Boolean = true
}
* 
*/


class NginxFileStorageTest extends FileStorageTest {
  
  val httpCfg = (new AsyncHttpClientConfig.Builder)
    .setConnectionTimeoutInMs(5000)
    .setRequestTimeoutInMs(5000)
    .setIdleConnectionTimeoutInMs(5000)
    .setIdleConnectionInPoolTimeoutInMs(5000)
    .build()
    
  val asyncHttpClient = new AsyncHttpClient(httpCfg)
  
  override val fileStorage: FileStorage = new NginxFileStorage(
    nginxPublicUrl = new URL("http://ci.singularex.com:10080/"),
    nginxWebDavUrl = new URL("http://ci.singularex.com:10090/"),
    httpClientConfig = Some(httpCfg)
  )
      
  override lazy val isFileStorageAvailable: Boolean =    
    Try(asyncHttpClient.prepareGet("http://ci.singularex.com:10090/").execute().get()) match {
      case Success(resp) if resp.getStatusCode() == 200 => true
      case _ => false
    }
  
  "Direct download url" should {
    "be provided and work" in {
      skipIfNotAvailable()
      val fileName = UUID.randomUUID.toString
      val file = File(testNamespace, fileName, FileContent(testFileContent), "image/jpeg")
      await(fileStorage.save(file))
      val loaded = await(fileStorage.load(testNamespace, fileName))
      loaded must beSome
      val url = await(fileStorage.getFileExternalUrl(testNamespace, fileName))
      MD5.digest(DownloadUtils.download(url)) must beEqualTo("f4526db885966605fe80cb15e02af94f")
    }
  }
}


class FsFileStorageTest extends FileStorageTest {
  
  val storagelPath = "/tmp/FileStorageTest"
  
  override val fileStorage: FileStorage = new FsFileStorage(storagelPath)
  
  var isFileStorageAvailableVar = true
  override def isFileStorageAvailable: Boolean = isFileStorageAvailableVar
  
  def config: Map[String, Any] = Map(
    "filestorage.type" -> "fs",
    "filestorage.fs.path" -> storagelPath    
  )  
  
  try { 
    new java.io.File(storagelPath).mkdirs()
  } catch {
    case _: Throwable => isFileStorageAvailableVar = false
  }
}
