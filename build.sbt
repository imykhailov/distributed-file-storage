import de.johoop.jacoco4sbt._

import JacocoPlugin._

name := "distributed-file-storage"

organization := "me.singularex"

version := "0.1-SNAPSHOT"

resolvers += "Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases/"


libraryDependencies ++= Seq(
  "org.scalaj" % "scalaj-time_2.10.2" % "0.7",
  "com.spatial4j" % "spatial4j" % "0.3",
  "com.datastax.cassandra" % "cassandra-driver-core" % "2.0.0-beta2" excludeAll(ExclusionRule(organization = "org.slf4j"), ExclusionRule(organization = "net.jpountz.lz4")),
  "net.jpountz.lz4" % "lz4" % "1.1.0",
  "org.slf4j" % "slf4j-api" % "1.7.7",
  "com.typesafe.play" %% "play-json" % "2.3.1",
  "com.ning" % "async-http-client" % "1.8.12",
  "org.slf4j" % "slf4j-api" % "1.7.5"
)

//Tests
libraryDependencies ++= Seq(
  "org.cassandraunit" % "cassandra-unit" % "1.2.0.1" % "test" excludeAll(ExclusionRule(organization = "org.slf4j"), ExclusionRule("org.apache.cassandra", "cassandra-all")),
  "org.apache.cassandra" % "cassandra-all" % "2.0.1" % "test" excludeAll(ExclusionRule(organization = "org.slf4j")),  
  "org.specs2" % "specs2_2.10" % "2.0-RC2" % "test",
  "org.slf4j" % "slf4j-simple" % "1.7.7" % "test"
)



scalacOptions ++= Seq(
    "-deprecation"
  , "-feature"
  , "-unchecked"
  , "-Xlint"
  , "-Yno-adapted-args"
  , "-Ywarn-all"
  , "-Ywarn-dead-code"
  , "-language:postfixOps"
  , "-language:implicitConversions"
)

net.virtualvoid.sbt.graph.Plugin.graphSettings

org.scalastyle.sbt.ScalastylePlugin.Settings

jacoco.settings

parallelExecution in jacoco.Config := false

(testOptions in Test) += Tests.Argument(TestFrameworks.ScalaTest, "-u", "target/test-reports")
